﻿use Football
go

declare @json nvarchar(max)
set @json = 
N'[
   {
     "Name": "Camp nou",
	 "City":"Барселона"
   },
   {
     "Name": "Динамо",
	 "City":"Минск"
   }
]'

exec dbo.usp_insert__Stadium @json;
go