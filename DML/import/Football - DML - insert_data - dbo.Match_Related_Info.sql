﻿use Football
go

declare @json nvarchar(max)
set @json = N'
{  
   "Participants":[  
      {  
         "Person":{  
            "FullName":"Тэйлор Э.",
            "Birthday":"01.01.1900"
         },
         "MatchParticipantType":"Судья",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Марко Силва",
            "Birthday":"07.12.1977"
         },
         "MatchParticipantType":"Тренер",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Маурицио Сарри",
            "Birthday":"01.10.1959"
         },
         "MatchParticipantType":"Тренер",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Бернард",
            "Birthday":"09.08.1992"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Эден Азар",
            "Birthday":"01.07.1991"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Андре Гомеш",
            "Birthday":"07.30.1993"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Маркос Алонсо",
            "Birthday":"12.28.1990"
         },
         "MatchParticipantType":"Защитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Идрисса Гуйе",
            "Birthday":"09.26.1989"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Кепа Аррисабалага",
            "Birthday":"10.03.1994"
         },
         "MatchParticipantType":"Вратарь",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Лукас Динь",
            "Birthday":"07.20.1993"
         },
         "MatchParticipantType":"Защитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Сесар Аспиликуэта",
            "Birthday":"08.28.1989"
         },
         "MatchParticipantType":"Защитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Доминик Кальверт-Льюин",
            "Birthday":"03.16.1997"
         },
         "MatchParticipantType":"Нападающий",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Росс Баркли",
            "Birthday":"12.05.1993"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Майкл Кин",
            "Birthday":"01.11.1993"
         },
         "MatchParticipantType":"Защитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Жоржиньо",
            "Birthday":"12.20.1991"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Шимус Коулмэн",
            "Birthday":"10.11.1988"
         },
         "MatchParticipantType":"Защитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Гонсало Игуаин",
            "Birthday":"12.10.1987"
         },
         "MatchParticipantType":"Нападающий",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Йерри Мина",
            "Birthday":"09.23.1994"
         },
         "MatchParticipantType":"Защитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Нголо Канте",
            "Birthday":"03.29.1991"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Джордан Пикфорд",
            "Birthday":"03.07.1994"
         },
         "MatchParticipantType":"Вратарь",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Давид Луис",
            "Birthday":"04.22.1987"
         },
         "MatchParticipantType":"Защитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Ричарлисон",
            "Birthday":"05.10.1997"
         },
         "MatchParticipantType":"Нападающий",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Педро",
            "Birthday":"07.28.1987"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Гильфи Сигурдссон",
            "Birthday":"09.09.1989"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Антонио Рудигер",
            "Birthday":"03.03.1993"
         },
         "MatchParticipantType":"Защитник",
         "IsMain":true
      },
      {  
         "Person":{  
            "FullName":"Ademola Lookman",
            "Birthday":"10.20.1997"
         },
         "MatchParticipantType":"Нападающий",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Каллам Hudson-Odoi",
            "Birthday":"11.07.2000"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Лейтон Бейнс",
            "Birthday":"12.11.1984"
         },
         "MatchParticipantType":"Защитник",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Виллиан",
            "Birthday":"08.09.1988"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Том Дэвис",
            "Birthday":"06.30.1998"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Оливье Жиру",
            "Birthday":"09.30.1986"
         },
         "MatchParticipantType":"Нападающий",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Мартен Стекеленбург",
            "Birthday":"09.22.1982"
         },
         "MatchParticipantType":"Вратарь",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Вильфредо Кабайеро",
            "Birthday":"09.28.1981"
         },
         "MatchParticipantType":"Вратарь",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Дженк Тосун",
            "Birthday":"06.07.1991"
         },
         "MatchParticipantType":"Нападающий",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Матео Ковачич",
            "Birthday":"05.06.1994"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Тео Уолкотт",
            "Birthday":"03.16.1989"
         },
         "MatchParticipantType":"Нападающий",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Андреас Кристенсен",
            "Birthday":"04.10.1996"
         },
         "MatchParticipantType":"Защитник",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Морган Шнайдерлин",
            "Birthday":"11.08.1989"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":false
      },
      {  
         "Person":{  
            "FullName":"Рубен Лофтус-Чик",
            "Birthday":"01.23.1996"
         },
         "MatchParticipantType":"Полузащитник",
         "IsMain":false
      }
   ],
   "Tour":{  
      "Name":"Тур 31",
      "StartDate":"03.16.2019",
      "EndDate":"03.17.2019",
      "Tournament":{  
         "TournamentName":"Премьер-лига",
         "StartDate":"01.01.1900",
         "Country":"Англия"
      }
   },
   "Date":"2019-03-17T19:30:00",
   "Stadium":{  
      "Name":"Гудисон Парк",
      "City":"Ливерпуль"
   },
   "Spectactors":39356,
   "Teams":[  
      {  
         "Name":"Эвертон",
         "Country":"Англия",
         "IsHomeTeam":true,
         "Score":2
      },
      {  
         "Name":"Челси",
         "Country":"Англия",
         "IsHomeTeam":false,
         "Score":0
      }
   ]}'

exec dbo.usp_insert__Match @json;
go