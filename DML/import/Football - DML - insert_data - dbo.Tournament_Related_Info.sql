﻿use Football
go

declare @json nvarchar(max)
set @json = 
N'{"TournamentName":"Премьер-лига","StartDate":"01.01.1900","EndDate":"01.01.1900","Country":"Англия","Tours":[{"Name":"Тур 31","StartDate":"03.16.2019","EndDate":"03.17.2019","Tournament":{"TournamentName":"Премьер-лига","StartDate":"01.01.1900","Country":"Англия"}},{"Name":"Тур 30","StartDate":"03.09.2019","EndDate":"03.10.2019","Tournament":{"TournamentName":"Премьер-лига","StartDate":"01.01.1900","Country":"Англия"}},{"Name":"Тур 29","StartDate":"03.02.2019","EndDate":"03.03.2019","Tournament":{"TournamentName":"Премьер-лига","StartDate":"01.01.1900","Country":"Англия"}},{"Name":"Тур 28","StartDate":"02.26.2019","EndDate":"02.27.2019","Tournament":{"TournamentName":"Премьер-лига","StartDate":"01.01.1900","Country":"Англия"}},{"Name":"Тур 27","StartDate":"02.22.2019","EndDate":"02.24.2019","Tournament":{"TournamentName":"Премьер-лига","StartDate":"01.01.1900","Country":"Англия"}},{"Name":"Тур 26","StartDate":"02.09.2019","EndDate":"02.11.2019","Tournament":{"TournamentName":"Премьер-лига","StartDate":"01.01.1900","Country":"Англия"}},{"Name":"Тур 25","StartDate":"02.02.2019","EndDate":"02.04.2019","Tournament":{"TournamentName":"Премьер-лига","StartDate":"01.01.1900","Country":"Англия"}},{"Name":"Тур 24","StartDate":"01.29.2019","EndDate":"01.30.2019","Tournament":{"TournamentName":"Премьер-лига","StartDate":"01.01.1900","Country":"Англия"}},{"Name":"Тур 23","StartDate":"01.19.2019","EndDate":"01.20.2019","Tournament":{"TournamentName":"Премьер-лига","StartDate":"01.01.1900","Country":"Англия"}},{"Name":"Тур 22","StartDate":"01.12.2019","EndDate":"01.14.2019","Tournament":{"TournamentName":"Премьер-лига","StartDate":"01.01.1900","Country":"Англия"}},{"Name":"Тур 21","StartDate":"01.01.2019","EndDate":"01.03.2019","Tournament":{"TournamentName":"Премьер-лига","StartDate":"01.01.1900","Country":"Англия"}}]}'

exec dbo.usp_insert__Tournament_Related_Info @json;
go

