Use Football
go

select 
(
	select c.[Name]
	from dbo.Country c
	for json path
) as [Countries],
(
	select ctt.[Name]
	from dbo.Card_Type_Template ctt
	for json path
) as [Cards],
(
	select ett.[Name]
	from dbo.Event_Type_Template ett
	for json path
) as [Events],
(
	select mtt.[Name]
	from dbo.Match_Time_Type_Template mtt
	for json path
) as [Times],
(
	select mptt.[Name]
	from dbo.Match_Participant_Type_Template mptt
	for json path
) as [Match_Participant_Types],
(
	select mtt.[Name]
	from dbo.Injury_Type_Template mtt
	for json path
) as [Injury_Types],
(
	select itt.[Name]
	from dbo.Person_Title_Template itt
	for json path
) as [Person_Titles],
(
	select mtt.[Name]
	from dbo.Movement_Type_Template mtt
	for json path
) as [Movement_Types]
for json path
