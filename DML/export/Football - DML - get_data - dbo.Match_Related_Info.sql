use Football
go

select	tt.Tour_Name as [Tour.Name],
		tt.Start_Date as [Tour.StartDate],
		tt.End_Date as [Tour.EndDate],
		t.Name as [Tour.Tournament.TournamentName],
		t.Start_Date as [Tour.Tournament.StartDate],
		c.Name as [Tour.Tournament.Country],
		m.Date_Time as [Date],
		s.Name as [Stadium.Name],
		s.City as [Stadium.City],
		m.Spectactors as [Spectactors],
		(
			select	tmt.Name as [Name],
					tmc.Name as [Country],
					tmx.Is_Home_Team as [IsHomeTeam],
					tmx.Score					
			from Team_Match_Xref tmx
			inner join Team tmt on tmt.Team_ID=tmx.Team_ID
			inner join Country tmc on tmc.Country_ID=tmt.Country_ID
			where tmx.Match_ID=m.Match_ID
			for json path
		) as [Teams],
		(
			select	mptt.Name as [MatchParticipantType],
					mpx.Is_Main as [IsMain],
					case p.Last_Name
						when '' then p.First_Name
						else p.First_Name + ' ' + p.Last_Name
					end as [Person.FullName],
					p.Birthday as [Person.Birthday]
			from Match_Participant_Xref mpx
			inner join Match_Participant_Type_Template mptt on mptt.Match_Participant_Type_ID = mpx.Match_Participant_Type_ID
			inner join Person p on p.Person_ID=mpx.Person_ID
			where mpx.Match_ID=m.Match_ID
			for json path
		) as [Participants]
from Match m
inner join Tournament_Tour tt on tt.Tournament_Tour_ID=m.Tournament_Tour_ID
inner join Tournament t on t.Tournament_ID=tt.Tournament_ID
inner join Country c on c.Country_ID = t.Country_ID
inner join Stadium s on s.Stadium_ID=m.Stadium_ID
for json path


