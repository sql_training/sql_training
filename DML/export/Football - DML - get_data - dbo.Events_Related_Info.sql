use Football
go

;with events_cte(
		Match_Event_ID,
		Match_ID,
		MatchStage,
		EventTemplate,
		MatchTimeSpan,
		[Person.FullName],
		[Person.Birthday],
		[Description]
) as 
(
	select	me.Match_Event_ID,
			Match_ID,
			mtt.Name as [MatchStage],
			ett.Name as [EventTemplate],
			me.Match_Time as [MatchTimeSpan],
			case pEvent.Last_Name
				when '' then pEvent.First_Name
				else pEvent.First_Name + ' ' + pEvent.Last_Name
			end as [Person.FullName],
			pEvent.Birthday as [Person.Birthday],
			me.Description as [Description]
	from Match_Event me
	inner join Match_Time_Type_Template mtt on mtt.Match_Time_Type_ID=me.Match_Time_Type_ID
	inner join Event_Type_Template ett on ett.Event_Type_ID=me.Event_Type_ID
	inner join Person pEvent on pEvent.Person_ID=me.Person_ID
)
select	tt.Tour_Name as [Match.Tour.Name],
		tt.Start_Date as [Match.Tour.StartDate],
		tt.End_Date as [Match.Tour.EndDate],
		t.Name as [Match.Tour.Tournament.TournamentName],
		t.Start_Date as [Match.Tour.Tournament.StartDate],
		c.Name as [Match.Tour.Tournament.Country],
		m.Date_Time as [Match.Date],
		s.Name as [Match.Stadium.Name],
		s.City as [Match.Stadium.City],
		(
			select	e.EventTemplate,
					e.MatchStage,
					e.MatchTimeSpan,
					e.[Person.FullName],
					e.[Person.Birthday],
					e.Description
			from events_cte e
			where e.Match_ID=m.Match_ID
			for json path
		) as [Events],
		(
			select	e2.EventTemplate as [Event.EventTemplate],
					e2.MatchStage as [Event.MatchStage],
					e2.MatchTimeSpan as [Event.MatchTimeSpan],
					e2.[Person.FullName] as [Event.Person.FullName],
					e2.[Person.Birthday] as [Event.Person.Birthday],
					ct.Name as [CardType],
					case p.Last_Name
						when '' then p.First_Name
						else p.First_Name + ' ' + p.Last_Name
					end as [Referee.FullName],
					p.Birthday as [Referee.Birthday]
			from events_cte e2
			inner join Match_Card_Xref mcx on mcx.Match_Event_ID=e2.Match_Event_ID
			inner join Card_Type_Template ct on ct.Card_Type_ID=mcx.Card_Type_ID
			inner join Person p on p.Person_ID=mcx.Person_ID
			where e2.Match_ID=m.Match_ID
			for json path
		) as [Cards],
		(
			select	e3.EventTemplate as [Event.EventTemplate],
					e3.MatchStage as [Event.MatchStage],
					e3.MatchTimeSpan as [Event.MatchTimeSpan],
					e3.[Person.FullName] as [Event.Person.FullName],
					e3.[Person.Birthday] as [Event.Person.Birthday],
					case p_from.Last_Name
						when '' then p_from.First_Name
						else p_from.First_Name + ' ' + p_from.Last_Name
					end as [From.FullName],
					p_from.Birthday as [From.Birthday],
					case p_to.Last_Name
						when '' then p_to.First_Name
						else p_to.First_Name + ' ' + p_to.Last_Name
					end as [To.FullName],
					p_to.Birthday as [To.Birthday],
					mpt_from.Name as [ParticipantTypeFrom],
					mpt_to.Name as [ParticipantTypeTo]
			from Match_Participant_Movement_Xref m_from
			inner join Match_Participant_Movement_Xref m_to on m_to.Match_Event_ID=m_from.Match_Event_ID and m_to.Has_Entered=1
			inner join Person p_from on p_from.Person_ID=m_from.Person_ID
			inner join Person p_to on p_to.Person_ID=m_to.Person_ID
			inner join events_cte e3 on e3.Match_Event_ID = m_from.Match_Event_ID
			inner join Match_Participant_Type_Template mpt_from on mpt_from.Match_Participant_Type_ID = m_from.Match_Participant_Type_ID
			inner join Match_Participant_Type_Template mpt_to on mpt_to.Match_Participant_Type_ID = m_to.Match_Participant_Type_ID
			where  e3.Match_ID=m.Match_ID and m_from.Has_Entered=0
			for json path
		) as [Movements]
from Match m
inner join Tournament_Tour tt on tt.Tournament_Tour_ID=m.Tournament_Tour_ID
inner join Tournament t on t.Tournament_ID=tt.Tournament_ID
inner join Country c on c.Country_ID = t.Country_ID
inner join Stadium s on s.Stadium_ID=m.Stadium_ID
for json path