use Football
go

select t.[Name], 
	   c.[Name] as [Country], 
	(
		select i.Mobile_Phone
		from dbo.Team_Info_History i
		where i.Team_ID = t.Team_ID
		for json path
	) as [Team_Info_History]
from dbo.Team t
inner join dbo.Country c on c.Country_ID = t.Country_ID
for json path