Use Football
go

declare @format nvarchar(10) = 'MM.dd.yyyy'

select 
		case
			when p.Last_Name = ' ' then p.First_Name
			else p.First_Name + ' ' + p.Last_Name 
		end as FullName,
	   format(p.Birthday, @format) as Birthday,
	   (
	     select itt.[Name],
		        format(i.Date_From, @format) as DateFrom,
				format(i.Date_To, @format)   as DateTo
		 from dbo.Injury i
		 inner join dbo.Injury_Type_Template itt on itt.Injury_Type_ID = i.Injury_Type_ID
		 where i.Person_ID = p.Person_ID
		 for json path
	   )  as [Injuries],

	   (
			select	team_from.Name as [TeamFrom.Name],
					c_from.Name as [TeamFrom.Country],
					team_to.Name as [TeamTo.Name],
					c_to.Name as [TeamTo.Country],
					ptt.Name as [Title],
					m_to.Date as [Date],
					mtt.Name as [MovementType],
					m_to.Number as [Number]
			from Person_Movement_Xref m_to
			left join Person_Movement_Xref m_from on m_from.Person_ID=m_to.Person_ID and m_from.Has_Entered = 0 
					and m_from.Date=m_to.Date 
					and m_from.Movement_Type_ID=m_to.Movement_Type_ID
			left join Team team_to on team_to.Team_ID=m_to.Team_ID
			left join Team team_from on team_from.Team_ID=m_from.Team_ID
			left join Country c_to on c_to.Country_ID=team_to.Country_ID
			left join Country c_from on c_from.Country_ID=team_from.Country_ID
			left join Person_Title_Template ptt on ptt.Person_Title_ID=m_to.Person_Title_ID
			left join Movement_Type_Template mtt on mtt.Movement_Type_ID = m_to.Movement_Type_ID
			where m_to.Has_Entered = 1 and m_to.Person_ID=p.Person_ID
			for json path				   
	   ) as [Movements],
	   (
		  select pih.Mobile_Phone, 
			     c.[Name]       as [Country]
		  from dbo.Person_Info_History pih 		 
		  inner join dbo.Country c on pih.Country_ID = c.Country_ID 
		  where p.Person_ID = pih.Person_ID
		  for json path
       )  as [Person_Info_History]
from dbo.Person p
for json path