use Football
go

declare @format nvarchar(10) = 'MM.dd.yyyy'

select t.[Name]						     as TournamentName, 
	   format(t.[Start_Date], @format)   as StartDate, 
	   format(t.End_Date, @format)       as EndDate,
	   c.[Name]                 	     as Country,
	   (
		 select tt.Tour_Name	                  as [Name],
				format(tt.[Start_Date], @format)  as StartDate,
				format(tt.End_Date, @format)      as EndDate
		 from dbo.Tournament_Tour tt
		 where tt.Tournament_ID = t.Tournament_ID
		 for json path
	   )			                     as Tours 
from dbo.Tournament t
inner join dbo.Country c on c.Country_ID = t.Country_ID
for json path