use Football
go

exec sp_MSForEachTable 'alter table ? nocheck constraint all' 
go 

exec sp_MSForEachTable 'delete from ?' 
go

exec sp_MSForEachTable 'alter table ? with check check constraint all' 
go 