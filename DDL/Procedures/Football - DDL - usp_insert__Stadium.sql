use Football
go

if object_id(N'usp_insert__Stadium') > 0
	drop procedure dbo.usp_insert__Stadium
go

create proc dbo.usp_insert__Stadium
(	
	@json nvarchar(max) = null,
	@DEBUG bit = null
)
as
begin
	set nocount on

	if((isjson(@json) = 1) and (@json is not null))
	begin
		insert into dbo.Stadium ([Name], City, Capacity)
		select s.[Name], s.City, s.Capacity
		from openjson(@json)
			with(
				[Name]		nvarchar(20)	'$.Name',
				City		nvarchar(20)	'$.City',
				Capacity	int				'$.Capacity'
			) s
		where dbo.usf_get__Stadium_ID__By_AK(s.[Name], s.City) is null
	end		
	else if(@DEBUG = 1)
		print 'json-string not parsed'
end
go