use Football
go

if object_id(N'usp_insert__Team_Related_Info') > 0
	drop procedure dbo.usp_insert__Team_Related_Info
go

create proc dbo.usp_insert__Team_Related_Info
(	
	@json nvarchar(max) = null
)
as
begin
	set nocount on

	declare @DEBUG bit = 0;

	if((isjson(@json) = 1) and (@json is not null))
	begin

		insert into dbo.Team(Country_ID, [Name])
		select c.Country_ID, t.[Name] 
		from openjson(@json) 
			with (
				[Name]          nvarchar(100)    N'$.Name',
				Country_Name    nvarchar(100)    N'$.Country',
				Info            nvarchar(max)    N'$.Team_Info_History' as json
			) t
		inner join dbo.Country c on c.[Name] = t.Country_Name
		where dbo.usf_get__Team_ID__By_AK(t.[Name], t.Country_Name) is null
	

		insert into dbo.Team_Info_History(Team_ID, Mobile_Phone)
		select  dbo.usf_get__Team_ID__By_AK(t.[Name], t.Country_Name), i.Mobile_Phone
		from openjson(@json) 
			with (
				[Name]          nvarchar(100)    N'$.Name',
				Country_Name    nvarchar(100)    N'$.Country',
				Info            nvarchar(max)    N'$.Team_Info_History' as json
			) t
		inner join dbo.Country c on c.[Name] = t.Country_Name
		cross apply openjson(t.Info) 
			with(
				Mobile_Phone    nvarchar(100)    N'$.Mobile_Phone',
				[Date]          datetime         N'$.Date'
			) i
		where dbo.usf_get__Team_Info_History_ID__By_AK(t.[Name], t.Country_Name, i.[Date]) is null
	end		
	else if(@DEBUG = 1)
		print 'json-string not parsed'
end
go