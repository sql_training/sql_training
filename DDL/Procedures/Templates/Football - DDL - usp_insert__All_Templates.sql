use Football
go

if object_id(N'usp_insert__Templates') > 0
	drop procedure dbo.usp_insert__Templates
go

create proc dbo.usp_insert__Templates
(	
	@json nvarchar(max) = null,
	@DEBUG bit = null
)
as
begin
	set nocount on

	if((isjson(@json) = 1) and (@json is not null))
	begin 

	    declare @Countries					nvarchar(max),
				@Cards						nvarchar(max),
				@Events						nvarchar(max),
				@Times						nvarchar(max),
				@Match_Participant_Types	nvarchar(max),
				@Injury_Types				nvarchar(max),
				@Person_Titles				nvarchar(max),
				@Movement_Types				nvarchar(max)


		select	@Countries = Countries, 
				@Cards = Cards, 
				@Events = [Events], 
				@Times = Times, 
				@Match_Participant_Types = Match_Participant_Types,
				@Injury_Types = Injury_Types,
				@Person_Titles = Person_Titles,
				@Movement_Types = Movement_Types

		from openjson(@json)
			with (
				Countries					nvarchar(max)    N'$.Countries'					as json,
				Cards						nvarchar(max)    N'$.Cards'						as json,
				[Events]					nvarchar(max)    N'$.Events'					as json,
				Times						nvarchar(max)    N'$.Times'						as json,
				Match_Participant_Types		nvarchar(max)    N'$.Match_Participant_Types'	as json,
				Injury_Types				nvarchar(max)    N'$.Injury_Types'				as json,
				Person_Titles				nvarchar(max)    N'$.Person_Titles'				as json,
				Movement_Types				nvarchar(max)    N'$.Movement_Types'			as json
			)

		exec dbo.usp_insert__Country							@Countries
		exec dbo.usp_insert__Card_Type_Template					@Cards
		exec dbo.usp_insert__Event_Type_Template				@Events
		exec dbo.usp_insert__Match_Time_Type_Template			@Times
		exec dbo.usp_insert__Match_Participant_Type_Template	@Match_Participant_Types
		exec dbo.usp_insert__Injury_Type_Template				@Injury_Types
		exec dbo.usp_insert__Person_Title_Template				@Person_Titles
		exec dbo.usp_insert__Movement_Type_Template				@Movement_Types


	end
	else if(@DEBUG = 1)
		print 'json-string not parsed'
end
go