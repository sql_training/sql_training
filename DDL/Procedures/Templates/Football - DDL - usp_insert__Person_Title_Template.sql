use Football
go

if object_id(N'usp_insert__Person_Title_Template') > 0
	drop procedure dbo.usp_insert__Person_Title_Template
go


create proc dbo.usp_insert__Person_Title_Template
(	
	@json nvarchar(max) = null,
	@DEBUG bit = null
)
as
begin
	set nocount on

	if((isjson(@json) = 1) and (@json is not null))
	begin
		insert into dbo.Person_Title_Template([Name])
		select *
		from openjson(@json)
			with (
				[Name]    nvarchar(40)    N'$.Name'
			)
		where dbo.usf_get__Person_Title_ID__By_AK([Name]) is null
	end
	else if(@DEBUG = 1)
		print 'json-string not parsed'
end
go