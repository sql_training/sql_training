use Football
go

if object_id(N'usp_insert__Match') > 0
	drop procedure dbo.usp_insert__Match
go


create proc dbo.usp_insert__Match
(	
	@json nvarchar(max) = null,
	@DEBUG bit = null
)
as
begin
	set nocount on

	if((isjson(@json) = 1) and (@json is not null))
	begin
		
		insert into Stadium ([Name], City)
		select	s.Stadium_Name, 
				s.Stadium_City
		from openjson(@json)
			with (
				Stadium_Name nvarchar(100) N'$.Stadium.Name',
				Stadium_City nvarchar(100) N'$.Stadium.City'
			) s
		where dbo.usf_get__Stadium_ID__By_AK(s.Stadium_Name,s.Stadium_City) is null

		declare @temp table
		(
			Tournament_Tour_ID	int,
			Date_Time			datetime,
			Stadium_ID			int,
			Spectactors			int,
			Teams				nvarchar(max),
			Participants		nvarchar(max)
		)

		insert into @temp (Tournament_Tour_ID, Date_Time, Stadium_ID, Spectactors, Teams, Participants)
		select	tt.Tournament_Tour_ID,
				m.Date_Time,
				s.Stadium_ID,
				m.Spectactors,
				m.Teams,
				m.Participants
		from openjson(@json)
			with (
				Tour_Name				nvarchar(40)    N'$.Tour.Name',
				Tour_Start_Date			date			N'$.Tour.StartDate',
				Tour_End_Date			date            N'$.Tour.StartDate',
				Tournament_Name			nvarchar(100)	N'$.Tour.Tournament.TournamentName',
				Tournament_Country		nvarchar(100)	N'$.Tour.Tournament.Country',
				Tournament_Start_Date	date			N'$.Tour.Tournament.StartDate',
				Date_Time				datetime		N'$.Date',
				Stadium_Name			nvarchar(100)	N'$.Stadium.Name',
				Stadium_City			nvarchar(100)	N'$.Stadium.City',
				Spectactors				int				N'$.Spectactors',
				Teams					nvarchar(max)	N'$.Teams' as json,
				Participants			nvarchar(max)	N'$.Participants' as json
			) m
		inner join Country c			on	c.[Name] = m.Tournament_Country
		inner join Tournament t			on	t.Country_ID = c.Country_ID 
											and t.[Name] = m.Tournament_Name 
											and t.[Start_Date] = m.Tournament_Start_Date
		inner join Tournament_Tour tt	on	tt.Tournament_ID=t.Tournament_ID 
											and tt.Tour_Name=m.Tour_Name
		inner join Stadium s			on	s.[Name] = m.Stadium_Name 
											and s.City = m.Stadium_City
		where dbo.usf_get__Match_ID__By_AK(tt.Tournament_Tour_ID, m.Date_Time, s.Stadium_ID) is null

		insert into dbo.Match (Tournament_Tour_ID, Date_Time, Stadium_ID, Spectactors)
		select Tournament_Tour_ID, Date_Time, Stadium_ID, Spectactors
		from @temp

		insert into dbo.Team_Match_Xref (Match_ID, Team_ID, Is_Home_Team, Score)
		select	m.Match_ID,
				tt.Team_ID,
				p.Is_Home_Team,
				p.Score
		from	@temp t
		cross apply openjson(t.Teams)
			with (
				Team_Name		nvarchar(100)	N'$.Name',
				Team_Country	nvarchar(100)	N'$.Country',
				Is_Home_Team	bit				N'$.IsHomeTeam',
				Score			int				N'$.Score'
			) p
		inner join Match m		on	m.Tournament_Tour_ID=t.Tournament_Tour_ID 
									and m.Date_Time=t.Date_Time 
									and m.Stadium_ID=t.Stadium_ID
		inner join Country c	on	c.[Name]=p.Team_Country
		inner join Team tt		on	tt.[Name]=p.Team_Name 
									and tt.Country_ID=c.Country_ID
		where dbo.usf_get__Team_Match_Xref_by_PK(m.Match_ID, tt.Team_ID) is null

		insert into dbo.Match_Participant_Xref(Match_ID, Person_ID,Match_Participant_Type_ID, Is_Main)
		select	m.Match_ID,
				dbo.usf_get_Person_ID__By_AK
					(
						dbo.usf_parse_Full_Name(p.FullName, 1),
						dbo.usf_parse_Full_Name(p.FullName, 2),
						p.Birthday
					) as Person_ID,
				mpt.Match_Participant_Type_ID,
				p.Is_Main
		from	@temp t
		cross apply openjson(t.Participants)
			with (
				FullName				nvarchar(100)	N'$.Person.FullName',
				Birthday				nvarchar(100)	N'$.Person.Birthday',
				Match_Participant_Type	nvarchar(100)	N'$.MatchParticipantType',
				Is_Main					bit				N'$.IsMain'
			) p
		inner join Match m on m.Tournament_Tour_ID = t.Tournament_Tour_ID 
							and m.Date_Time = t.Date_Time
							and m.Stadium_ID = t.Stadium_ID
		inner join Match_Participant_Type_Template mpt on mpt.[Name] = p.Match_Participant_Type
		where dbo.usf_get__Match_Participant_Xref_by_PK
				(
					dbo.usf_get_Person_ID__By_AK
						(
							dbo.usf_parse_Full_Name(p.FullName, 1),
							dbo.usf_parse_Full_Name(p.FullName, 2), 
							p.Birthday
						),
					m.Match_ID
				) is null
	end
	else if(@DEBUG = 1)
		print 'json-string not parsed'
end
go