use Football
go

if object_id(N'usp_insert__Tournament_Related_Info') > 0
	drop procedure dbo.usp_insert__Tournament_Related_Info
go

create proc dbo.usp_insert__Tournament_Related_Info
(	
	@json nvarchar(max) = null,
	@DEBUG bit = null
)
as
begin
	set nocount on

	if((isjson(@json) = 1) and (@json is not null))
	begin
		insert into dbo.Tournament(Country_ID, [Name], [Start_Date], End_Date)
		select c.Country_ID, t.[Name], t.StartDate, t.EndDate
		from openjson(@json)
			with (
				CountryName        nvarchar(100)    N'$.Country',
				[Name]             nvarchar(100)    N'$.TournamentName',
				StartDate		   date		        N'$.StartDate',
				EndDate            date             N'$.EndDate'
			) t
		inner join Country c on c.[Name] = t.CountryName	
		where dbo.usf_get__Tournament_ID__By_AK(t.[Name], t.StartDate, t.CountryName) is null	

		insert into dbo.Tournament_Tour(Tournament_ID, Tour_Name, [Start_Date], End_Date)
		select tr.Tournament_ID, tt.[Name], tt.StartDate, tt.EndDate
		from openjson(@json)
			with (
				CountryName			nvarchar(100)	N'$.Country',
				Tours				nvarchar(max)	N'$.Tours'	as json
			) t
		cross apply openjson(t.Tours)
			with (
				[Name]				nvarchar(100)	N'$.Name',
				StartDate			date			N'$.StartDate',
				EndDate				date			N'$.EndDate'
			) tt
		inner join dbo.Country c on c.[Name] = t.CountryName
		inner join dbo.Tournament tr on tr.Country_ID = c.Country_ID
		where dbo.usf_get__Tournament_Tour_ID__By_AK(tr.Tournament_ID, tt.[Name]) is null 


	end		
	else if(@DEBUG = 1)
		print 'json-string not parsed'
end
go