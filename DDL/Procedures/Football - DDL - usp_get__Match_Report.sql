use Football
go

if object_id(N'usp_get__Match_Report') > 0
	drop procedure dbo.usp_get__Match_Report
go

create proc dbo.usp_get__Match_Report
as
begin
	select 
		m.Match_ID,
		t.[Name]								              as [Tournament],
		tt.Tour_Name 							              as [Tour],
		m.Date_Time                                           as [Match_Datetime],
		dbo.usf_get__Match_Teams(m.Match_ID, 1)               as [First_Team],
		dbo.usf_get__Match_Teams(m.Match_ID, 0)               as [Second_Team],
		dbo.usf_get__Match_Scores(m.Match_ID, 1)              as [Scores_First_Team],
		dbo.usf_get__Match_Scores(m.Match_ID, 0)              as [Scores_Second_Team],
		dbo.usf_get__Match_Duration(m.Match_ID)			      as [Match_Duration],
		dbo.usf_get__Referee_By_Match(m.Match_ID)             as [Referee],
		dbo.usf_get__Is_Match_Had_Penalties(m.Match_ID)       as [Had_Penalties],
		dbo.usf_get__Cards_Count(m.Match_ID, 1, 0)            as [Yellow_Cards_First_Team],
		dbo.usf_get__Cards_Count(m.Match_ID, 1, 1)            as [Yellow_Cards_Second_Team],
		dbo.usf_get__Cards_Count(m.Match_ID, 2, 0)            as [Red_Cards_First_Team],
		dbo.usf_get__Cards_Count(m.Match_ID, 2, 1)            as [Red_Cards_Second_Team],
		dbo.usf_get__Match_Replacements_Number(m.Match_ID, 0) as [Replacements_First_Team],
		dbo.usf_get__Match_Replacements_Number(m.Match_ID, 1) as [Replacements_Second_Team]
	from dbo.[Match] m
	inner join dbo.Tournament_Tour tt 
		on tt.Tournament_Tour_ID = m.Tournament_Tour_ID
	inner join dbo.Tournament t 
		on t.Tournament_ID = tt.Tournament_ID
end
go