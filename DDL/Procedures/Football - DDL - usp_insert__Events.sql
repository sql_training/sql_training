use Football
go

if object_id(N'usp_insert__Events') > 0
	drop procedure dbo.usp_insert__Events
go


create proc dbo.usp_insert__Events
(	
	@json nvarchar(max) = null,
	@DEBUG bit = null
)
as
begin
	set nocount on

	if((isjson(@json) = 1) and (@json is not null))
	begin
		
		declare @temp table
		(
			Match_ID int,
			Match_Events nvarchar(max),
			Match_Cards nvarchar(max),
			Match_Movements nvarchar(max)
		)

		insert into @temp
		select	mt.Match_ID,
				m.Match_Events,
				m.Match_Cards,
				m.Match_Movements
		
		from openjson(@json)
			with (
					Tour_Name				nvarchar(40)    N'$.Match.Tour.Name',
					Tournament_Name			nvarchar(100)	N'$.Match.Tour.Tournament.TournamentName',
					Tournament_Country		nvarchar(100)	N'$.Match.Tour.Tournament.Country',
					Tournament_Start_Date	date			N'$.Match.Tour.Tournament.StartDate',
					Match_Date_Time			datetime		N'$.Match.Date',
					Stadium_Name			nvarchar(100)	N'$.Match.Stadium.Name',
					Stadium_City			nvarchar(100)	N'$.Match.Stadium.City',
					Match_Events			nvarchar(max)	N'$.Events'		as json,
					Match_Cards				nvarchar(max)	N'$.Cards'		as json,
					Match_Movements			nvarchar(max)	N'$.Movements'	as json
				) m
		inner join Country c on c.[Name] = m.Tournament_Country
		inner join Tournament t on t.Country_ID = c.Country_ID and t.[Name] = m.Tournament_Name and t.[Start_Date] = m.Tournament_Start_Date
		inner join Tournament_Tour tt on tt.Tournament_ID = t.Tournament_ID and tt.Tour_Name = m.Tour_Name
		inner join Stadium s on s.[Name] = m.Stadium_Name and s.City = m.Stadium_City
		inner join Match mt on mt.Tournament_Tour_ID = tt.Tournament_Tour_ID and mt.Stadium_ID = s.Stadium_ID and mt.Date_Time = m.Match_Date_Time

		insert into dbo.Match_Event(Person_ID, Match_Time_Type_ID, Event_Type_ID, Match_ID, Match_Time, Description)
		select	dbo.usf_get_Person_ID__By_AK
				(
					dbo.usf_parse_Full_Name(e.FullName, 1),
					dbo.usf_parse_Full_Name(e.FullName, 2), 
					e.Birthday
				) as Person_ID,
				mtt.Match_Time_Type_ID,
				et.Event_Type_ID,
				t.Match_ID,
				e.Match_Time,
				isnull(e.[Description],'') as [Description]
		from @temp t
		cross apply openjson(t.Match_Events)
			with (
					FullName		nvarchar(100)	N'$.Person.FullName',
					Birthday		nvarchar(100)	N'$.Person.Birthday',
					Event_Template	nvarchar(100)	N'$.EventTemplate',
					Match_Stage		nvarchar(100)	N'$.MatchStage',
					Match_Time		int				N'$.MatchTimeSpan',
					[Description]	nvarchar(4000)	N'$.Description'
			) e
		inner join Event_Type_Template et on et.[Name] = e.Event_Template
		inner join Match_Time_Type_Template mtt on mtt.[Name] = e.Match_Stage

		where dbo.usf_get__Match_Event_ID_by_AK(
				mtt.Match_Time_Type_ID, 
				et.Event_Type_ID,
				dbo.usf_get_Person_ID__By_AK(dbo.usf_parse_Full_Name(e.FullName, 1),dbo.usf_parse_Full_Name(e.FullName, 2), e.Birthday),
				t.Match_ID,
				e.Match_Time
				) is null

		insert into dbo.Match_Card_Xref (Person_ID, Match_Event_ID, Card_Type_ID)
		select	dbo.usf_get_Person_ID__By_AK
				(
					dbo.usf_parse_Full_Name(c.Referee_FullName, 1),
					dbo.usf_parse_Full_Name(c.Referee_FullName, 2), 
					c.Referee_Birthday
				) as Person_ID,
				e.Match_Event_ID,
				ct.Card_Type_ID
		from @temp t
		cross apply openjson(t.Match_Cards)
			with (
					Referee_FullName			nvarchar(100)	N'$.Referee.FullName',
					Referee_Birthday			nvarchar(100)	N'$.Referee.Birthday',
					Card_Type					nvarchar(100)	N'$.CardType',
					Event_Person_Full_Name		nvarchar(100)	N'$.Event.Person.FullName',
					Event_Person_Birthday		nvarchar(100)	N'$.Event.Person.Birthday',
					Event_Template				nvarchar(100)	N'$.Event.EventTemplate',
					Match_Stage					nvarchar(100)	N'$.Event.MatchStage',
					Match_Time					int				N'$.Event.MatchTimeSpan',
					[Description]				nvarchar(4000)	N'$.Event.Description'
			) c
		inner join Card_Type_Template ct on ct.Name = c.Card_Type
		inner join Match_Time_Type_Template mtt on mtt.Name = c.Match_Stage
		inner join Event_Type_Template ett on ett.Name = c.Event_Template
		inner join Match_Event e on e.Event_Type_ID = ett.Event_Type_ID
									and e.Match_Time = c.Match_Time
									and e.Match_Time_Type_ID = mtt.Match_Time_Type_ID
									and e.Match_ID = t.Match_ID
									and e.Person_ID = dbo.usf_get_Person_ID__By_AK
										(
											dbo.usf_parse_Full_Name(c.Event_Person_Full_Name, 1),
											dbo.usf_parse_Full_Name(c.Event_Person_Full_Name, 2), 
											c.Event_Person_Birthday
										)
		where dbo.usf_get__Match_Card_Xref__By_PK
				(
					e.Match_Event_ID, 
					dbo.usf_get_Person_ID__By_AK
					 (
						dbo.usf_parse_Full_Name(c.Referee_FullName, 1),
						dbo.usf_parse_Full_Name(c.Referee_FullName, 2), 
						c.Referee_Birthday
					 ),
					ct.Card_Type_ID
				) is null



		;with movement_cte (From_Person_ID, To_Person_ID, From_Participant_Type_ID, To_Participant_Type_ID, Match_Event_ID) 
			as (
				select	dbo.usf_get_Person_ID__By_AK
							(
								dbo.usf_parse_Full_Name(c.From_Full_Name, 1),
								dbo.usf_parse_Full_Name(c.From_Full_Name, 2),
								c.From_Birthday
							) as From_Person_ID,
						dbo.usf_get_Person_ID__By_AK
							(
								dbo.usf_parse_Full_Name(c.To_Full_Name, 1),
								dbo.usf_parse_Full_Name(c.To_Full_Name, 2),
								c.To_Birthday
							) as To_Person_ID,
						dbo.usf_get__Match_Participant_Type_ID__By_AK(c.From_Participant_Type) as From_Participant_Type_ID,
						dbo.usf_get__Match_Participant_Type_ID__By_AK(c.To_Participant_Type) as To_Participant_Type_ID,
						e.Match_Event_ID
				from @temp t
				cross apply openjson(t.Match_Movements)
						with (
								From_Full_Name			nvarchar(100)	N'$.From.FullName',
								From_Birthday			nvarchar(100)	N'$.From.Birthday',
								To_Full_Name			nvarchar(100)	N'$.To.FullName',
								To_Birthday				nvarchar(100)	N'$.To.Birthday',
								From_Participant_Type	nvarchar(100)	N'$.ParticipantTypeFrom',
								To_Participant_Type		nvarchar(100)	N'$.ParticipantTypeTo',
								Event_Person_Full_Name	nvarchar(100)	N'$.Event.Person.FullName',
								Event_Person_Birthday	nvarchar(100)	N'$.Event.Person.Birthday',
								Event_Template			nvarchar(100)	N'$.Event.EventTemplate',
								Match_Stage				nvarchar(100)	N'$.Event.MatchStage',
								Match_Time				int				N'$.Event.MatchTimeSpan',
								[Description]			nvarchar(4000)	N'$.Event.Description'
							) c
				inner join Match_Time_Type_Template mtt on mtt.Name = c.Match_Stage
				inner join Event_Type_Template ett on ett.Name = c.Event_Template
				inner join Match_Event e on e.Event_Type_ID = ett.Event_Type_ID
											and e.Match_Time = c.Match_Time
											and e.Match_Time_Type_ID = mtt.Match_Time_Type_ID
											and e.Match_ID = t.Match_ID
											and e.Person_ID = dbo.usf_get_Person_ID__By_AK
												(
													dbo.usf_parse_Full_Name(c.Event_Person_Full_Name, 1),
													dbo.usf_parse_Full_Name(c.Event_Person_Full_Name, 2), 
													c.Event_Person_Birthday
												)
				)

		insert into Match_Participant_Movement_Xref (Match_Event_ID, Person_ID, Has_Entered, Match_Participant_Type_ID)
		select	t.Match_Event_ID, t.Person_ID, t.Has_Entered, t.Match_Participant_Type_ID
		from (
			select	Match_Event_ID, 
					From_Person_ID as Person_ID,
					0 as Has_Entered, 
					From_Participant_Type_ID as Match_Participant_Type_ID
			from movement_cte
			union 
			select	Match_Event_ID,
					To_Person_ID as Person_ID, 
					1 as Has_Entered, 
					To_Participant_Type_ID as Match_Participant_Type_ID
			from movement_cte
		) t
		where dbo.usf_get__Match_Participant_Movement_Xref_by_PK(t.Match_Event_ID, t.Person_ID, t.Has_Entered) is null

	end
	else if(@DEBUG = 1)
		print 'json-string not parsed'
end
go