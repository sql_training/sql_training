use Football
go

if object_id(N'usp_insert__Person_Related_Info') > 0
	drop procedure dbo.usp_insert__Person_Related_Info
go


create proc dbo.usp_insert__Person_Related_Info
(	
	@json nvarchar(max) = null,
	@DEBUG bit = null
)
as
begin
	set nocount on

	if((isjson(@json) = 1) and (@json is not null))
	begin
		declare @temp table
		(
			First_Name			nvarchar(40),
			Last_Name			nvarchar(40),
			Birthday			date,
			Person_Info_History nvarchar(max),
			Injuries			nvarchar(max),
			Movements			nvarchar(max)
		)

		insert into @temp (First_Name, Last_Name, Birthday, Person_Info_History, Injuries, Movements)
		select dbo.usf_parse_Full_Name(p.Full_Name, 1) as First_Name,
		       dbo.usf_parse_Full_Name(p.Full_Name, 2) as Last_Name,
		       p.Birthday,
			   p.Person_Info_History,
			   p.Injuries,
			   p.Movements
		from openjson(@json)
			with (
				Full_Name				nvarchar(100)		N'$.FullName',
				Birthday				date				N'$.Birthday',
				Person_Info_History		nvarchar(max)		N'$.PersonInfoHistory' as json,
				Injuries				nvarchar(max)		N'$.Injuries'		   as json,
				Movements				nvarchar(max)		N'$.Movements'		   as json
			) p
		where dbo.usf_get_Person_ID__By_AK
				(
					dbo.usf_parse_Full_Name(p.Full_Name, 1), 
					dbo.usf_parse_Full_Name(p.Full_Name, 2), 
					p.Birthday
				) is null

		insert  into dbo.Person(First_Name, Last_Name, Birthday) 
		select First_Name, Last_Name, Birthday
		from @temp

		insert into dbo.Person_Info_History(Person_ID, Country_ID, Mobile_Phone)
		select dbo.usf_get_Person_ID__By_AK(t.First_Name, t.Last_Name, t.BirthDay), 
			   c.Country_ID,
			   pih.Mobile_phone
		from @temp t
		cross apply openjson(t.Person_Info_History)
		    with(
				Mobile_phone       nvarchar(20)     N'$.MobilePhone',
				Country_Name       nvarchar(100)    N'$.Country',
				[Date]             date             N'$.Date'
			) pih
		inner join Country as c on c.[Name] = pih.Country_Name
		where dbo.usf_get__Person_Info_History_ID__By_AK(dbo.usf_get_Person_ID__By_AK(t.First_Name, t.Last_Name, t.BirthDay), c.Country_ID, pih.[Date]) is null
		
		insert into dbo.Injury(Person_ID, Injury_Type_ID, Date_From, Date_To)
		select dbo.usf_get_Person_ID__By_AK(t.First_Name, t.Last_Name, t.BirthDay), 
		       Injury_Type_ID, 
			   i.DateFrom, 
			   i.DateTo
		from @temp t
		cross apply openjson(t.Injuries)
		    with(
				[Name]			       nvarchar(20)     N'$.Name',
				DateFrom		       nvarchar(100)    N'$.DateFrom',
				DateTo                 date             N'$.DateTo'
			) i
		inner join dbo.Injury_Type_Template it on it.[Name] = i.[Name] 
		where dbo.usf_get__Injury_ID__By_AK(dbo.usf_get_Person_ID__By_AK(t.First_Name, t.Last_Name, t.BirthDay), Injury_Type_ID, i.DateFrom, i.DateTo) is null


		insert into dbo.Person_Movement_Xref(Team_ID, Person_ID, Person_Title_ID, Has_Entered, [Date], Movement_Type_ID, Number)
		select t.Team_ID,
			   dbo.usf_get_Person_ID__By_AK(p.First_Name, p.Last_Name, p.BirthDay),
			   dbo.usf_get__Person_Title_ID__By_AK(m.[Role]) as Person_Title_ID, 
			   0 as Has_Entered,
			   m.[Date], 
			   dbo.usf_get__Movement_Type_ID__By_AK(m.[Type]) as Movement_Type_ID,
			   m.Number
		from @temp p
		cross apply openjson(p.Movements)
		    with(
				[Date]			       date		        N'$.Date',
				[Type]				   nvarchar(40)	    N'$.MovementType',
				TeamFrom		       nvarchar(40)     N'$.TeamFrom.Name',
				TeamCountry			   nvarchar(100)	N'$.TeamFrom.Country',
				[Role]		           nvarchar(20)     N'$.Title',
				Number				   tinyint			N'$.Number'
			) m
		inner join dbo.Country c on c.[Name] = m.TeamCountry
		inner join dbo.Team t on t.[Name] = m.TeamFrom
		where t.Country_ID = c.Country_ID
				and dbo.usf_get__Person_Movement_Xref_by_PK
					(
						t.Team_ID,
						dbo.usf_get_Person_ID__By_AK(p.First_Name, p.Last_Name, p.BirthDay),
						0,
						m.[Date]
					) is null

		union all

		select t.Team_ID,
			   dbo.usf_get_Person_ID__By_AK(p.First_Name, p.Last_Name, p.BirthDay),
			   dbo.usf_get__Person_Title_ID__By_AK(m.[Role])as Person_Title_ID,  
			   1 as Has_Entered,
			   m.[Date], 
			   dbo.usf_get__Movement_Type_ID__By_AK(m.[Type]) as Movement_Type_ID,
			   m.Number
		from @temp p
		cross apply openjson(p.Movements)
		    with(
				[Date]			       date		        N'$.Date',
				[Type]				   nvarchar(40)	    N'$.MovementType',
				TeamTo  		       nvarchar(40)     N'$.TeamTo.Name',
				TeamCountry			   nvarchar(100)	N'$.TeamTo.Country',
				[Role]		           nvarchar(20)     N'$.Title',
				Number				   tinyint			N'$.Number'
			) m
		inner join dbo.Country c on c.[Name] = m.TeamCountry
		inner join dbo.Team t on t.[Name] = m.TeamTo
		where t.Country_ID = c.Country_ID
			and dbo.usf_get__Person_Movement_Xref_by_PK
					(
						t.Team_ID,
						dbo.usf_get_Person_ID__By_AK(p.First_Name, p.Last_Name, p.BirthDay),
						1,
						m.[Date]
					) is null
	end
	else if(@DEBUG = 1)
		print 'json-string not parsed'
end
go