use Football
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Match_Participant_Movement_Xref__Match_Event'
)
alter table dbo.Match_Participant_Movement_Xref
drop constraint fk_Match_Participant_Movement_Xref__Match_Event
go

alter table dbo.Match_Participant_Movement_Xref
add constraint fk_Match_Participant_Movement_Xref__Match_Event
foreign key (Match_Event_ID) references dbo.Match_Event(Match_Event_ID)
on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Match_Participant_Movement_Xref__Person'
)
alter table dbo.Match_Participant_Movement_Xref
drop constraint fk_Match_Participant_Movement_Xref__Person
go

alter table dbo.Match_Participant_Movement_Xref
add constraint fk_Match_Participant_Movement_Xref__Person
foreign key (Person_ID) references dbo.Person(Person_ID)
on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Match_Participant_Movement_Xref__Participant_Type'
)
alter table dbo.Match_Participant_Movement_Xref
drop constraint fk_Match_Participant_Movement_Xref__Participant_Type
go

alter table dbo.Match_Participant_Movement_Xref
add constraint fk_Match_Participant_Movement_Xref__Participant_Type
foreign key (Match_Participant_Type_ID) references dbo.Match_Participant_Type_Template(Match_Participant_Type_ID)
on delete cascade
go