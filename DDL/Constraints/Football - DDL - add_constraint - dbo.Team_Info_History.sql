use Football
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Team_Info_History__Team'
)
alter table dbo.Team_Info_History
drop constraint fk_Team_Info_History__Team
go


alter table dbo.Team_Info_History
add constraint fk_Team_Info_History__Team
foreign key (Team_ID) references dbo.Team(Team_ID)
on delete cascade
go


if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Team_Info_History__Team__Date'
)
alter table dbo.Team_Info_History
drop constraint ak_Team_Info_History__Team__Date
go

alter table dbo.Team_Info_History
add constraint ak_Team_Info_History__Team__Date unique (Team_ID, [Date])
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='df_Team_Info_History__Date'
)
alter table dbo.Team_Info_History
drop constraint df_Team_Info_History__Date
go

alter table dbo.Team_Info_History
add constraint df_Team_Info_History__Date default getdate() for [Date]
go
