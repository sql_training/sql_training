use Football
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Injury__Person'
)
alter table dbo.Injury
drop constraint fk_Injury__Person
go


alter table dbo.Injury
add constraint fk_Injury__Person
foreign key (Person_Id) references dbo.Person(Person_ID)
on delete cascade
go


if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Injury__Injury_Type_Template'
)
alter table dbo.Injury
drop constraint fk_Injury__Injury_Type_Template
go


alter table dbo.Injury
add constraint fk_Injury__Injury_Type_Template
foreign key (Injury_Type_ID) references dbo.Injury_Type_Template(Injury_Type_ID)
on delete cascade
go


if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Injury__Person__DateFrom__DateTo'
)
alter table dbo.Injury
drop constraint ak_Injury__Person__DateFrom__DateTo
go

alter table dbo.Injury
add constraint ak_Injury__Person__DateFrom__DateTo unique (Person_ID, Date_From, Date_To)
go
