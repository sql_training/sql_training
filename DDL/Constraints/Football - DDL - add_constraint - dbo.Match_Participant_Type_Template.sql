use Football
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Match_Participant_Type_Template__Name'
)
alter table dbo.Match_Participant_Type_Template
drop constraint ak_Match_Participant_Type_Template__Name
go

alter table dbo.Match_Participant_Type_Template
add constraint ak_Match_Participant_Type_Template__Name unique ([Name])
go