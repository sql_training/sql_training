use Football
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='fk_Tournament_Tour__Tournament'
)
alter table dbo.Tournament_Tour
drop constraint fk_Tournament_Tour__Tournament
go

alter table dbo.Tournament_Tour
add constraint fk_Tournament_Tour__Tournament
foreign key (Tournament_ID) references dbo.Tournament(Tournament_ID)
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Tournament_Tour__Tournament_ID__Tour_Name'
)
alter table dbo.Tournament_Tour
drop constraint ak_Tournament_Tour__Tournament_ID__Tour_Name
go

alter table dbo.Tournament_Tour
add constraint ak_Tournament_Tour__Tournament_ID__Tour_Name unique (Tournament_ID, Tour_Name)
go
