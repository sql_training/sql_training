use Football
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Match_Participant_Xref__Person'
)
alter table dbo.Match_Participant_Xref
drop constraint fk_Match_Participant_Xref__Person
go

alter table dbo.Match_Participant_Xref
add constraint fk_Match_Participant_Xref__Person
foreign key (Person_ID) references dbo.Person(Person_ID)
on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Match_Participant_Xref__Match'
)
alter table dbo.Match_Participant_Xref
drop constraint fk_Match_Participant_Xref__Match
go

alter table dbo.Match_Participant_Xref
add constraint fk_Match_Participant_Xref__Match
foreign key (Match_ID) references dbo.[Match](Match_ID)
on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Match_Participant_Xref__Participant_Type_Template'
)
alter table dbo.Match_Participant_Xref
drop constraint fk_Match_Participant_Xref__Participant_Type_Template
go


alter table dbo.Match_Participant_Xref
add constraint fk_Match_Participant_Xref__Participant_Type_Template
foreign key (Match_Participant_Type_ID) references dbo.Match_Participant_Type_Template(Match_Participant_Type_ID)
on delete cascade
go