use Football
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Movement_Type_Template__Name'
)
alter table dbo.Movement_Type_Template
drop constraint ak_Movement_Type_Template__Name
go

alter table dbo.Movement_Type_Template
add constraint ak_Movement_Type_Template__Name unique ([Name])
go