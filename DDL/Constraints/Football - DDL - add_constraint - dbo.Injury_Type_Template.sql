use Football
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Injury_Type_Template__Name'
)
alter table dbo.Injury_Type_Template
drop constraint ak_Injury_Type_Template__Name
go

alter table dbo.Injury_Type_Template
add constraint ak_Injury_Type_Template__Name unique ([Name])
go