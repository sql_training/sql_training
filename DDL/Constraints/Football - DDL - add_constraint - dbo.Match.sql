use Football
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Match__Tournament_Tour__Stadium__Date_Time'
)
alter table dbo.[Match]
drop constraint ak_Match__Tournament_Tour__Stadium__Date_Time
go


alter table dbo.[Match]
add constraint ak_Match__Tournament_Tour__Stadium__Date_Time 
unique (Tournament_Tour_ID, Stadium_ID, Date_Time)
go


if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Match__Tournament_Tour'
)
alter table dbo.[Match]
drop constraint fk_Match__Tournament_Tour
go

alter table dbo.[Match]
add constraint fk_Match__Tournament_Tour
foreign key (Tournament_Tour_ID) references dbo.Tournament_Tour(Tournament_Tour_ID)
on delete cascade
go


if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Match__Stadium'
)
alter table dbo.[Match]
drop constraint fk_Match__Stadium
go


alter table dbo.[Match]
add constraint fk_Match__Stadium
foreign key (Stadium_ID) references dbo.Stadium(Stadium_ID)
on delete cascade
go