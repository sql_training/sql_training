use Football
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Person__Last_Name__First_Name__Birthday'
)
alter table dbo.Person
drop constraint ak_Person__Last_Name__First_Name__Birthday
go

alter table dbo.Person
add constraint ak_Person__Last_Name__First_Name__Birthday unique (Last_Name, First_Name, Birthday)
go
