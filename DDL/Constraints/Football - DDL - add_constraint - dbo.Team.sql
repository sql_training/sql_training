﻿use Football
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Team__Country'
)
alter table dbo.Team
drop constraint fk_Team__Country
go

alter table dbo.Team
add constraint fk_Team__Country 
foreign key (Country_ID) references dbo.Country(Country_ID)
on delete cascade
go


if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Team__Country__Name'
)
alter table dbo.Team
drop constraint ak_Team__Country__Name
go

alter table dbo.Team
add constraint ak_Team__Country__Name unique (Country_ID, [Name])
go