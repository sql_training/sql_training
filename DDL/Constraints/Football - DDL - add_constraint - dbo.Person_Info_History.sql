use Football
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Person_Info_History__Person'
)
alter table dbo.Person_Info_History
drop constraint fk_Person_Info_History__Person
go


alter table dbo.Person_Info_History
add constraint fk_Person_Info_History__Person
foreign key (Person_Id) references dbo.Person(Person_ID)
on delete cascade
go


if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Person_Info_History__Country'
)
alter table dbo.Person_Info_History
drop constraint fk_Person_Info_History__Country
go


alter table dbo.Person_Info_History
add constraint fk_Person_Info_History__Country
foreign key (Country_ID) references dbo.Country(Country_ID)
on delete cascade
go


if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Person_Info_History__Person__Date'
)
alter table dbo.Person_Info_History
drop constraint ak_Person_Info_History__Person__Date
go

alter table dbo.Person_Info_History
add constraint ak_Person_Info_History__Person__Date unique (Person_ID, [Date])
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='df_Person_Info_History__Date'
)
alter table dbo.Person_Info_History
drop constraint df_Person_Info_History__Date
go

alter table dbo.Person_Info_History
add constraint df_Person_Info_History__Date default getdate() for [Date]
go