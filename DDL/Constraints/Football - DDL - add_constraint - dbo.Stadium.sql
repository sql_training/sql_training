﻿use Football
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Stadium__City__Name'
)
alter table dbo.Stadium
drop constraint ak_Stadium__City__Name
go

alter table dbo.Stadium
add constraint ak_Stadium__City__Name unique (City, [Name])
go