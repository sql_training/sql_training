use Football
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Person_Movement_Xref__Team'
)
alter table dbo.Person_Movement_Xref
drop constraint fk_Person_Movement_Xref__Team
go

alter table dbo.Person_Movement_Xref
add constraint fk_Person_Movement_Xref__Team
foreign key (Team_ID) references dbo.Team(Team_ID)
on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Person_Movement_Xref__Person'
)
alter table dbo.Person_Movement_Xref
drop constraint fk_Person_Movement_Xref__Person
go

alter table dbo.Person_Movement_Xref
add constraint fk_Person_Movement_Xref__Person
foreign key (Person_ID) references dbo.Person(Person_ID)
on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Person_Movement_Xref__Person_Title_Template'
)
alter table dbo.Person_Movement_Xref
drop constraint fk_Person_Movement_Xref__Person_Title_Template
go

alter table dbo.Person_Movement_Xref
add constraint fk_Person_Movement_Xref__Person_Title_Template
foreign key (Person_Title_ID) references dbo.Person_Title_Template(Person_Title_ID)
on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Person_Movement_Xref__Movement_Type_Template'
)
alter table dbo.Person_Movement_Xref
drop constraint fk_Person_Movement_Xref__Movement_Type_Template
go

alter table dbo.Person_Movement_Xref
add constraint fk_Person_Movement_Xref__Movement_Type_Template
foreign key (Movement_Type_ID) references dbo.Movement_Type_Template(Movement_Type_ID)
on delete cascade
go


