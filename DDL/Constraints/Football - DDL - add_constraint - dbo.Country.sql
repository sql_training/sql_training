use Football
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Country__Name'
)
alter table dbo.Country
drop constraint ak_Country__Name
go

alter table dbo.Country
add constraint ak_Country__Name unique ([Name])
go