use Football
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Tournament__Name__Time__Country'
)
alter table dbo.Tournament
drop constraint ak_Tournament__Name__Time__Country
go

alter table dbo.Tournament
add constraint ak_Tournament__Name__Time__Country unique ([Name], [Start_Date], Country_ID)
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Tournament__Country'
)
alter table dbo.Tournament
drop constraint fk_Tournament__Country
go


alter table dbo.Tournament
add constraint fk_Tournament__Country
foreign key (Country_ID) references dbo.Country(Country_ID)
on delete cascade
go