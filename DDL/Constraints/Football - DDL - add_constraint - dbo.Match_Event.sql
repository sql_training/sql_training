use Football
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Match_Event__Person__Match__Match_Time__Match_Time_Template'
)
alter table dbo.Match_Event
drop constraint ak_Match_Event__Person__Match__Match_Time__Match_Time_Template
go


alter table dbo.Match_Event
add constraint ak_Match_Event__Person__Match__Match_Time__Match_Time_Template 
unique (Person_ID, Match_Time_Type_ID, Event_Type_ID, Match_ID, Match_Time)
go


if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Match_Event__Person'
)
alter table dbo.Match_Event
drop constraint fk_Match_Event__Person
go

alter table dbo.Match_Event
add constraint fk_Match_Event__Person
foreign key (Person_ID) references dbo.Person(Person_ID)
go


if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Match_Event__Match_Time_Template'
)
alter table dbo.Match_Event
drop constraint fk_Match_Event__Match_Time_Template
go

alter table dbo.Match_Event
add constraint fk_Match_Event__Match_Time_Template
foreign key (Match_Time_Type_ID) references dbo.Match_Time_Type_Template(Match_Time_Type_ID)
on delete cascade
go


if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Match_Event__Event_Template'
)
alter table dbo.Match_Event
drop constraint fk_Match_Event__Event_Template
go

alter table dbo.Match_Event
add constraint fk_Match_Event__Event_Template
foreign key (Event_Type_ID) references dbo.Event_Type_Template(Event_Type_ID)
on delete cascade
go


if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Match_Event__Match'
)
alter table dbo.Match_Event
drop constraint fk_Match_Event__Match
go

alter table dbo.Match_Event
add constraint fk_Match_Event__Match
foreign key (Match_ID) references dbo.[Match](Match_ID)
on delete cascade
go