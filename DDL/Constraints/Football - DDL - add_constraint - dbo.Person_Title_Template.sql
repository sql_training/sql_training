use Football
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Person_Title_Template__Name'
)
alter table dbo.Person_Title_Template
drop constraint ak_Person_Title_Template__Name
go

alter table dbo.Person_Title_Template
add constraint ak_Person_Title_Template__Name unique ([Name])
go