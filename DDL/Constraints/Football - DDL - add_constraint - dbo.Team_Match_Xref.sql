use Football
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Team_Match_Xref__Match'
)
alter table dbo.[Team_Match_Xref]
drop constraint fk_Team_Match_Xref__Match
go

alter table dbo.[Team_Match_Xref]
add constraint fk_Team_Match_Xref__Match
foreign key (Match_ID) references dbo.[Match](Match_ID)
on delete cascade
go


if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Team_Match_Xref__Team'
)
alter table dbo.[Team_Match_Xref]
drop constraint fk_Team_Match_Xref__Team
go


alter table dbo.[Team_Match_Xref]
add constraint fk_Team_Match_Xref__Team
foreign key (Team_ID) references dbo.[Team](Team_ID)
on delete cascade
go