use Football
go

if exists
(
	select 1 
	from sys.tables
	where Name = 'Match_Event'
)
drop table dbo.Match_Event
go

create table dbo.Match_Event
(
	Match_Event_ID            int               not null    identity,
	Person_ID                 int               not null,
	Match_Time_Type_ID        tinyint           not null,
	Event_Type_ID             tinyint           not null,
	Match_ID                  int               not null,
	Match_Time                tinyint           not null,
	[Description]             nvarchar(4000)

	constraint pk_Match_Event primary key (Match_Event_ID),
)
go