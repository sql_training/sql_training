use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Match_Time_Type_Template'
)
drop table dbo.Match_Time_Type_Template
go

create table dbo.Match_Time_Type_Template
(
	Match_Time_Type_ID        tinyint       not null    identity,
	[Name]                    nvarchar(40)  not null,

	constraint pk_Match_Time_Type_Template primary key (Match_Time_Type_ID),
)
go
