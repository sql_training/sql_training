use football
go

if exists
(
	select 1 
	from sys.tables
	where Name = 'Match_Participant_Xref'
)
drop table dbo.Match_Participant_Xref
go

create table dbo.Match_Participant_Xref
(
	Person_ID					int,
	Match_ID					int,
	Match_Participant_Type_ID	tinyint    not null,
	Is_Main						bit        not null,
	
	constraint pk_Match_Participant_Xref primary key (Person_ID, Match_ID),
)
go