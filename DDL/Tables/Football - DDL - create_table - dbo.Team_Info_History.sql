use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Team_Info_History'
)
drop table dbo.Team_Info_History
go

create table dbo.Team_Info_History
(
	Team_Info_History_ID    smallint         not null    identity,
	Team_ID                 int              not null,
	Mobile_Phone            nvarchar(20),
	[Date]                  datetime         not null,

	constraint pk_Team_Info_History primary key (Team_Info_History_ID),
)
go