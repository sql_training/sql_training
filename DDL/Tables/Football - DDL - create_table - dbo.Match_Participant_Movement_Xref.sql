use Football
go

if exists
(
	select 1 
	from sys.tables
	where Name = 'Match_Participant_Movement_Xref'
)
drop table dbo.Match_Participant_Movement_Xref
go

create table dbo.Match_Participant_Movement_Xref
(
	Match_Event_ID				int not null,
	Person_ID					int not null,
	Has_Entered					bit not null,
	Match_Participant_Type_ID	tinyint,

	constraint pk_Match_Participant_Movement_Xref primary key (Match_Event_ID, Person_ID, Has_Entered)
)
go