use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Match'
)
drop table dbo.[Match]
go

create table dbo.[Match]
(
	Match_ID              int         not null    identity,
	Tournament_Tour_ID    smallint,
	Date_Time             datetime    not null,
	Stadium_ID		      smallint	  not null,
	Spectactors			  int,

	constraint pk_Match primary key (Match_ID),
)
go