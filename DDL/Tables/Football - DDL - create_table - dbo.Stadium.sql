use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Stadium'
)
drop table dbo.Stadium
go

create table dbo.Stadium
(
	Stadium_ID		smallint		not null    identity,
	[Name]			nvarchar(40)	not null,
	City			nvarchar(40)	not null,
	Capacity		int,

	constraint pk_Stadium primary key (Stadium_ID),
)
go
