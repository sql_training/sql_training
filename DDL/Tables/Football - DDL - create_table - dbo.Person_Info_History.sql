use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Person_Info_History'
)
drop table dbo.Person_Info_History
go

create table dbo.Person_Info_History
(
    Person_Info_History_ID     int             not null     identity,
	Person_ID                  int             not null,
	Mobile_Phone               nvarchar(20),
	Country_ID                 smallint        not null,
	[Date]                     date,

	constraint pk_Person_Info_History primary key (Person_Info_History_ID),
)
go