use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Injury'
)
drop table dbo.Injury
go

create table dbo.Injury
(
	Injury_ID			int			not null   identity,
	Person_ID			int			not null,
	Injury_Type_ID		tinyint		not null,
	Date_From			date		not null,
	Date_To				date

	constraint pk_Injury primary key (Injury_ID),
)
go