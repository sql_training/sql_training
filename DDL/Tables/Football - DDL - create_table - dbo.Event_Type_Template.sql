use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Event_Type_Template'
)
drop table dbo.Event_Type_Template
go

create table dbo.Event_Type_Template
(
	Event_Type_ID    tinyint          not null    identity,
	[Name]                    nvarchar(100)    not null,

	constraint pk_Event_Type_Template primary key (Event_Type_ID),
)
go
