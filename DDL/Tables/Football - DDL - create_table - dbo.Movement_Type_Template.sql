use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Movement_Type_Template'
)
drop table dbo.Movement_Type_Template
go

create table dbo.Movement_Type_Template
(
	Movement_Type_ID	tinyint       not null    identity,
	[Name]				nvarchar(40)  not null,

	constraint pk_Movement_Type_Template primary key (Movement_Type_ID),
)
go
