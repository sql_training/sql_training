use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Tournament_Tour'
)
drop table dbo.Tournament_Tour
go

create table dbo.Tournament_Tour
(
	Tournament_Tour_ID    smallint      not null    identity,
	Tournament_ID         smallint		not null,
	Tour_Name			  nvarchar(100) not null,
	[Start_Date]          date          not null,
	End_Date              date,

	constraint pk_Tournament_Tour primary key (Tournament_Tour_ID),
)
go
