use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Match_Card_Xref'
)
drop table dbo.Match_Card_Xref
go

create table dbo.Match_Card_Xref
(
	Card_Type_ID	tinyint,
	Person_ID			int,
	Match_Event_ID      int,

	constraint pk_Match_Card_Xref primary key (Card_Type_ID, Person_ID, Match_Event_ID),
)
go