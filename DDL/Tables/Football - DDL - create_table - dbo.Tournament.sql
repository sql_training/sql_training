use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Tournament'
)
drop table dbo.Tournament
go

create table dbo.Tournament
(
	Tournament_ID      smallint         not null    identity,
	Country_ID		   smallint         not null,         
	[Name]             nvarchar(100)    not null,
	[Start_Date]       date		        not null,
	End_Date           date,

	constraint pk_Tournament primary key (Tournament_ID),
)
go