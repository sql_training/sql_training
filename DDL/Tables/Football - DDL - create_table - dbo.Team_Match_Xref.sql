use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Team_Match_Xref'
)
drop table dbo.[Team_Match_Xref]
go

create table dbo.[Team_Match_Xref]
(
	Match_ID              int,
	Team_ID               int,
	Is_Home_Team		  bit,
	Score			      tinyint,

	constraint pk_Team_Match_Xref primary key (Match_ID, Team_ID),
)
go