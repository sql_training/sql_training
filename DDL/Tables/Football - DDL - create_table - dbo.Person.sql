use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Person'
)
drop table dbo.Person
go

create table dbo.Person
(
	Person_ID      int             not null   identity,
	First_Name     nvarchar(40)    not null,
	Last_Name      nvarchar(40)    not null,
	Birthday       date

	constraint pk_Person primary key (Person_ID),
)
go