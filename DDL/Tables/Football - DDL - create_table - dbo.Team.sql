use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Team'
)
drop table dbo.Team
go

create table dbo.Team
(
	Team_ID       int              not null    identity,
	Country_ID    smallint,
	[Name]        nvarchar(100)    not null,

	constraint pk_Team primary key (Team_ID),
)
go
