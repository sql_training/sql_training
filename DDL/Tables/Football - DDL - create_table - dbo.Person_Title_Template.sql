use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Person_Title_Template'
)
drop table dbo.Person_Title_Template
go

create table dbo.Person_Title_Template
(
	Person_Title_ID		tinyint       not null    identity,
	[Name]				nvarchar(20)  not null,

	constraint pk_Person_Title_Template primary key (Person_Title_ID),
)
go
