use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Injury_Type_Template'
)
drop table dbo.Injury_Type_Template
go

create table dbo.Injury_Type_Template
(
	Injury_Type_ID		tinyint         not null    identity,
	[Name]						nvarchar(40)    not null,

	constraint pk_Injury_Type_Template primary key (Injury_Type_ID),
)
go
