use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Card_Type_Template'
)
drop table dbo.Card_Type_Template
go

create table dbo.Card_Type_Template
(
	Card_Type_ID    tinyint		not null    identity,
	[Name]                   nvarchar(40)   not null,

	constraint pk_Card_Type_Template primary key (Card_Type_ID),
)
go
