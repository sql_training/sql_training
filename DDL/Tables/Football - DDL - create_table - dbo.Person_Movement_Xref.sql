use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Person_Movement_Xref'
)
drop table dbo.Person_Movement_Xref
go

create table dbo.Person_Movement_Xref
(
	Team_ID					int			not null,
	Person_ID				int			not null,
	Person_Title_ID			tinyint		not null,
	Has_Entered				bit			not null,
	[Date]					date		not null,
	Movement_Type_ID		tinyint,
	Number					tinyint,
	constraint pk_Person_Movement_Xref primary key (Team_ID,Person_ID,Has_Entered,[Date]),
)
go
