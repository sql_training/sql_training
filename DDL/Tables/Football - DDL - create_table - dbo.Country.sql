use Football
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Country'
)
drop table dbo.Country
go

create table dbo.Country
(
	Country_ID  smallint	     not null    identity,
	[Name]      nvarchar(100)    not null,

	constraint pk_Country primary key (Country_ID),
)
go