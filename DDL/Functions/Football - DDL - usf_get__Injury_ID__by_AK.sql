use Football
go

if object_id(N'usf_get__Injury_ID__By_AK') > 0
	drop function dbo.usf_get__Injury_ID__By_AK
go

create function dbo.usf_get__Injury_ID__By_AK(@Person_ID int, @Injury_Type_ID tinyint, @DateFrom date, @DateTo date)
returns int
as begin
	return (
		select i.Injury_ID
		from dbo.Injury i
		where i.Date_From = @DateFrom 
			and i.Injury_Type_ID = @Injury_Type_ID
			and i.Person_ID = @Person_ID
			and i.Date_To = @DateTo
	)
end
go