use Football
go

if exists
(
	select 1 
	from sys.Objects 
	where Object_Id = Object_Id(N'usf_get__Referee_Match_Xref_by_PK')
)
drop function dbo.usf_get__Referee_Match_Xref_by_PK
go

create function dbo.usf_get__Referee_Match_Xref_by_PK(@Referee_ID int, @Match_ID int)
returns int
as begin
	return (select t.Referee_ID
			from dbo.Referee_Match_Xref t
			where t.Match_ID = @Match_ID and t.Referee_ID = @Referee_ID)
end
go