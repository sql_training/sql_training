

use Football
go

if object_id(N'usf_get__Match_Replacements_Number') > 0
	drop function dbo.usf_get__Match_Replacements_Number
go

create function dbo.usf_get__Match_Replacements_Number(@Match_ID int, @Is_Home_Team bit)
returns nvarchar(20)
as
begin
		declare @return int = 
		(
			select count(1) as [count]
			from (
					select distinct mpm.Match_Event_ID, tm.Team_ID
					from dbo.Match_Event me
					inner join dbo.Match_Participant_Movement_Xref mpm
						on mpm.Match_Event_ID = me.Match_Event_ID
					inner join dbo.Team_Match_Xref tm
						on tm.Match_ID = me.Match_ID
					inner join dbo.Person_Movement_Xref pm
						on pm.Team_ID = tm.Team_ID 
						and pm.Person_ID = me.Person_ID
					where me.Match_ID = @Match_ID
						and mpm.Has_Entered = 1
						and tm.Is_Home_Team = @Is_Home_Team
				) s
			group by s.Team_ID
		)

		return isnull(@return, 0)
end
go