use Football
go

if exists
(
	select 1 
	from sys.Objects 
	where Object_Id = Object_Id(N'usf_get__Match_Players_Xref_by_PK')
)
drop function dbo.usf_get__Match_Players_Xref_by_PK
go

create function dbo.usf_get__Match_Players_Xref_by_PK(@Player_ID int, @Match_ID int)
returns int
as begin
	return (select p.Player_ID
			from dbo.Match_Players_Xref p
			where p.Player_ID = @Player_ID and p.Match_ID = @Match_ID)
end
go