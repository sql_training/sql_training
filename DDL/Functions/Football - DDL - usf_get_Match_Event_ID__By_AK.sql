use Football
go

if object_id(N'usf_get__Match_Event_ID_by_AK') > 0
	drop function dbo.usf_get__Match_Event_ID_by_AK
go

create function dbo.usf_get__Match_Event_ID_by_AK(@Match_Time_Template_ID tinyint, @Event_Type_ID tinyint, @Person_ID int, @Match_Id int, @Match_Time int)
returns int
as begin
	return (
		select e.Match_Event_ID
		from dbo.Match_Event e
		where e.Match_Time_Type_ID = @Match_Time_Template_ID 
			and e.Event_Type_ID = @Event_Type_ID 
			and e.Person_ID = @Person_ID
			and e.Match_ID = @Match_Id 
			and e.Match_Time = @Match_Time
	)
end
go