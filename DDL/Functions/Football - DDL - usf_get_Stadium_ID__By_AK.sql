use Football
go

if object_id(N'usf_get__Stadium_ID__By_AK') > 0
	drop function dbo.usf_get__Stadium_ID__By_AK
go

create function dbo.usf_get__Stadium_ID__By_AK(@Name nvarchar(20), @City nvarchar(20))
returns int
as begin
	return (
		select s.Stadium_ID
		from dbo.Stadium s
		where s.[Name] = @Name 
			and s.City = @City
	)
end
go