use Football
go

if object_id(N'usf_get__Tournament_Tour_ID__By_AK') > 0
	drop function dbo.usf_get__Tournament_Tour_ID__By_AK
go

create function dbo.usf_get__Tournament_Tour_ID__By_AK(@Tournament_ID int, @Tour_Name nvarchar(100))
returns int
as begin
	return (
		select t.Tournament_Tour_ID
		from dbo.Tournament_Tour t				
		where t.Tournament_ID = @Tournament_ID
			and t.Tour_Name = @Tour_Name
	)
end
go

