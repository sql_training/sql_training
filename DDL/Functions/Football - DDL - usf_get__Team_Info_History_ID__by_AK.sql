use Football
go

if object_id(N'usf_get__Team_Info_History_ID__By_AK') > 0
	drop function dbo.usf_get__Team_Info_History_ID__By_AK
go

create function dbo.usf_get__Team_Info_History_ID__By_AK(@Name nvarchar(100), @Country_Name nvarchar(100), @Date datetime)
returns int
as begin
	return (
		select t.Team_Info_History_ID
		from dbo.Team_Info_History t
		where t.Date = @Date 
			and t.Team_ID = dbo.usf_get__Team_ID__By_AK(@Name, @Country_Name)
	)
end
go