use Football
go

if object_id(N'usf_get__Team_ID__By_AK') > 0
	drop function dbo.usf_get__Team_ID__By_AK
go

create function dbo.usf_get__Team_ID__By_AK(@Name nvarchar(100), @Country_Name nvarchar(100))
returns int
as begin
	return (
		select t.Team_ID
		from dbo.Team t
		inner join dbo.Country c on c.Name = @Country_Name
		where t.[Name] = @Name
	)
end
go