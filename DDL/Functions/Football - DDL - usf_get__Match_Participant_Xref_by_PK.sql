use Football
go

if object_id(N'usf_get__Match_Participant_Xref_by_PK') > 0
	drop function dbo.usf_get__Match_Participant_Xref_by_PK
go

create function dbo.usf_get__Match_Participant_Xref_by_PK(@Person_ID int, @Match_ID int)
returns int
as begin
	return (
		select p.Person_ID
		from dbo.Match_Participant_Xref p
		where p.Person_ID = @Person_ID 
			and p.Match_ID = @Match_ID
	)
end
go