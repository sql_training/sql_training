use Football
go

if object_id(N'usf_get__Match_ID__By_AK') > 0
	drop function dbo.usf_get__Match_ID__By_AK
go

create function dbo.usf_get__Match_ID__By_AK(@Tournament_Tour_ID int, @Date_Time datetime, @Stadium_ID int)
returns int
as begin
	return (
		select m.Match_ID
		from dbo.[Match] m
		where m.Tournament_Tour_ID = @Tournament_Tour_ID
			and m.Date_Time = @Date_Time
			and m.Stadium_ID = @Stadium_ID
	)
end
go