use Football
go

if object_id(N'usf_get__Match_Card_Xref__By_PK') > 0
	drop function dbo.usf_get__Match_Card_Xref__By_PK
go

create function dbo.usf_get__Match_Card_Xref__By_PK(@Match_Event_ID int, @Person_ID smallint, @Card_Type_ID smallint)
returns int
as
begin
	return (
		select c.Person_ID
		from dbo.Match_Card_Xref c
		where c.Card_Type_ID = @Card_Type_ID 
			and c.Match_Event_ID = @Match_Event_ID 
			and c.Person_ID = @Person_ID
	)
end
go