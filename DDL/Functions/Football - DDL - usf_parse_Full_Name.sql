use Football
go

if object_id(N'usf_parse_Full_Name') > 0
	drop function dbo.usf_parse_Full_Name
go

create function dbo.usf_parse_Full_Name(@FullName nvarchar(100), @Part int)
returns nvarchar(100)
as begin

	declare @words table 
	(
		Part int identity, 
		Word nvarchar(100)
	)

	insert into @words
	select value 
	from string_split(@FullName, ' ')

	declare @ret nvarchar(100) = 
	(
		select Word 
		from @words 
		where Part=@Part
	)

	return isnull(@ret,'')

end
go