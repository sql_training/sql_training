use Football
go

if object_id(N'usf_get__Match_Scores') > 0
	drop function dbo.usf_get__Match_Scores
go

create function dbo.usf_get__Match_Scores(@Match_ID int, @Is_Home_Team bit)
returns nvarchar(20)
as
begin
	return
		(
			select tm.Score
			from dbo.Team_Match_Xref tm
			where tm.Match_ID =@Match_ID
				and tm.Is_Home_Team = @Is_Home_Team
		)		
end
go