use Football
go

if object_id(N'usf_get__Tournament_ID__By_AK') > 0
	drop function dbo.usf_get__Tournament_ID__By_AK
go

create function dbo.usf_get__Tournament_ID__By_AK(@Name nvarchar(100), @Start_Date date, @Country_Name nvarchar(100))
returns int
as begin
	return (
		select t.Tournament_ID
		from dbo.Tournament t
		inner join dbo.Country c on c.[Name] = @Country_Name
		where t.[Name] = @Name 
			and t.[Start_Date] = @Start_Date
	)
end
go