use Football
go

if object_id(N'usf_get__Person_Info_History_ID__By_AK') > 0
	drop function dbo.usf_get__Person_Info_History_ID__By_AK
go

create function dbo.usf_get__Person_Info_History_ID__By_AK(@Person_ID int, @Country_ID smallint, @Date datetime)
returns int
as begin
	return (
		select t.Person_Info_History_ID
		from dbo.Person_Info_History t
		where t.Person_ID = @Person_ID
			and t.Country_ID = @Country_ID 
			and (t.[Date] = @Date or (t.[Date] is null and @Date is null))
	)
end
go