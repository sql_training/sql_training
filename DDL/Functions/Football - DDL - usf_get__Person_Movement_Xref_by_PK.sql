use Football
go

if object_id(N'usf_get__Person_Movement_Xref_by_PK') > 0
	drop function dbo.usf_get__Person_Movement_Xref_by_PK
go

create function dbo.usf_get__Person_Movement_Xref_by_PK(@Team_ID int, @Person_ID int, @Has_Entered int, @Date date)
returns int
as begin
	return (
		select p.Person_ID
		from dbo.Person_Movement_Xref p
		where p.Team_ID = @Team_ID 
			and p.Person_ID = @Person_ID 
			and p.Has_Entered = @Has_Entered
			and p.[Date] = @Date
	)
end
go