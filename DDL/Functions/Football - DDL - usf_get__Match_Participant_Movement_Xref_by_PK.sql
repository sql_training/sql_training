use Football
go

if object_id(N'usf_get__Match_Participant_Movement_Xref_by_PK') > 0
	drop function dbo.usf_get__Match_Participant_Movement_Xref_by_PK
go

create function dbo.usf_get__Match_Participant_Movement_Xref_by_PK(@Match_Event_ID int, @Person_ID int, @Has_Entered int)
returns int
as begin
	return (
		select p.Person_ID
		from dbo.Match_Participant_Movement_Xref p
		where p.Match_Event_ID = @Match_Event_ID 
			and p.Person_ID = @Person_ID 
			and p.Has_Entered = @Has_Entered
	)
end
go