use Football
go

if object_id(N'usf_get_Person_ID__By_AK') > 0
	drop function dbo.usf_get_Person_ID__By_AK
go

create function dbo.usf_get_Person_ID__By_AK(@First_Name nvarchar(40), @Last_Name nvarchar(40), @Birthday date)
returns int
as
begin
	return (
		select per.Person_ID
		from dbo.Person as per
		where per.First_Name = @First_Name 
			and per.Last_Name = @Last_Name 
			and per.Birthday = @Birthday
	)
end
go