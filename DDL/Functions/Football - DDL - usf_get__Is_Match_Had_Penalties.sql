﻿use Football
go

if object_id(N'usf_get__Is_Match_Had_Penalties') > 0
	drop function dbo.usf_get__Is_Match_Had_Penalties
go

create function dbo.usf_get__Is_Match_Had_Penalties(@Match_ID int)
returns nvarchar(20)
as
begin
	return
		(
		isnull(
		(
			select 1
			from dbo.Match_Event me
			inner join dbo.Event_Type_Template ett
				on me.Event_Type_ID = ett.Event_Type_ID
			where me.Match_ID = @Match_ID
				and ett.[Name] like N'%[Пп]енальти'
		), 0) 
	)				
end
go