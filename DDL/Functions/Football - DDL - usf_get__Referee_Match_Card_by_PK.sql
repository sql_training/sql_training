use Football
go

if exists
(
	select 1 
	from sys.Objects 
	where Object_Id = Object_Id(N'usf_get__Referee__Match_Card__By_PK')
)
drop function dbo.usf_get__Referee__Match_Card__By_PK
go

create function dbo.usf_get__Referee__Match_Card__By_PK(@Match_Event_ID int, @Card_Template_ID smallint, @Referee_ID smallint)
returns int
as
begin
	return (select c.Referee_ID from dbo.Match_Card_Xref c
	        where c.Card_Template_ID = @Card_Template_ID and
			      c.Match_Event_ID = @Match_Event_ID and
				  c.Referee_ID = @Referee_ID)
end
go