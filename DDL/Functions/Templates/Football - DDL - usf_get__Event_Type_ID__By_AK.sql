use Football
go

if object_id(N'usf_get__Event_Type_ID__By_AK') > 0
	drop function dbo.usf_get__Event_Type_ID__By_AK
go

create function dbo.usf_get__Event_Type_ID__By_AK(@Name nvarchar(100))
returns int
as begin
	return (
		select e.Event_Type_ID
		from dbo.Event_Type_Template e
		where e.[Name] = @Name
	)
end
go