use Football
go

if object_id(N'usf_get__Country_ID__By_AK') > 0
	drop function dbo.usf_get__Country_ID__By_AK
go

create function dbo.usf_get__Country_ID__By_AK(@Name nvarchar(100))
returns int
as begin
	return (
		select c.Country_ID
		from dbo.Country c
		where c.[Name] = @Name
	)
end
go