use Football
go

if object_id(N'usf_get__Injury_Type_ID__By_AK') > 0
	drop function dbo.usf_get__Injury_Type_ID__By_AK
go

create function dbo.usf_get__Injury_Type_ID__By_AK(@Name nvarchar(100))
returns int
as begin
	return (select t.Injury_Type_ID
			from dbo.Injury_Type_Template t
			where t.[Name] = @Name)
end
go