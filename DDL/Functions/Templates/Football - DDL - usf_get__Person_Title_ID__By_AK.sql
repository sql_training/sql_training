use Football
go

if object_id(N'usf_get__Person_Title_ID__By_AK') > 0
	drop function dbo.usf_get__Person_Title_ID__By_AK
go

create function dbo.usf_get__Person_Title_ID__By_AK(@Name nvarchar(100))
returns int
as begin
	return (
		select t.Person_Title_ID
		from dbo.Person_Title_Template t
		where t.[Name] = @Name
	)
end
go