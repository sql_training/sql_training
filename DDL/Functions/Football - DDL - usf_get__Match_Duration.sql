﻿use Football
go

if object_id(N'usf_get__Match_Duration') > 0
	drop function dbo.usf_get__Match_Duration
go

create function dbo.usf_get__Match_Duration(@Match_ID int)
returns tinyint
as
begin
	declare @return tinyint
	set @return = 
	(
		select me.Match_Time
		from dbo.Match_Event me
		inner join dbo.Event_Type_Template ett
			on me.Event_Type_ID = ett.Event_Type_ID
		where me.Match_ID = @Match_ID
			and ett.[Name] like N'Свисток'
	)	
	return isnull(@return, 90)
end
go