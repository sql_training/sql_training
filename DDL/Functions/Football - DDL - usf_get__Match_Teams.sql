use Football
go

if object_id(N'usf_get__Match_Teams') > 0
	drop function dbo.usf_get__Match_Teams
go

create function dbo.usf_get__Match_Teams(@Match_ID int, @Is_Home_Team bit)
returns nvarchar(100)
as
begin
	return
		(
			select t.[Name]
			from dbo.Team_Match_Xref tm
			inner join dbo.Team t
				on tm.Team_ID = t.Team_ID
			where tm.Match_ID = @Match_ID
				and tm.Is_Home_Team = @Is_Home_Team
		)			
end
go