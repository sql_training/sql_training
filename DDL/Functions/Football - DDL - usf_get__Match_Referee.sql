﻿use Football
go

if object_id(N'usf_get__Referee_By_Match') > 0
	drop function dbo.usf_get__Referee_By_Match
go

create function dbo.usf_get__Referee_By_Match(@Match_ID int)
returns nvarchar(100)
as
begin
	return
		(
		select case
					when p.Last_Name = ' ' then p.First_Name
					else p.First_Name + ' ' + p.Last_Name 
			   end as FullName
		from dbo.Match_Participant_Xref mp
		inner join dbo.Match_Participant_Type_Template mpt
			on mpt.Match_Participant_Type_ID = mp.Match_Participant_Type_ID
		inner join dbo.Person p
			on mp.Person_ID = p.Person_ID
		where mp.Match_ID = @Match_ID 
				and mpt.[Name] like N'Судья'
		)	
end
go