use Football
go

if object_id(N'usf_get__Team_Match_Xref_by_PK') > 0
	drop function dbo.usf_get__Team_Match_Xref_by_PK
go

create function dbo.usf_get__Team_Match_Xref_by_PK(@Match_ID int, @Team_ID int)
returns int
as begin
	return (
		select t.Match_ID
		from dbo.Team_Match_Xref t
		where t.Match_ID = @Match_ID 
			and t.Team_ID = @Team_ID
	)
end
go